import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string ="Top 10 Movies" ;  
  name ='hello world';
  show: boolean = true;
  movie:Movie[]=[  
    {title:'Zootopia',director:'Byron Howard, Rich Moore',cast:'Idris Elba, Ginnifer Goodwin, Jason Bateman',releaseDate:'March 4, 2016'},  
    {title:'Batman v Superman: Dawn of Justice',director:'Zack Snyder',cast:'Ben Affleck, Henry Cavill, Amy Adams',releaseDate:'March 25, 2016'},  
    {title:'Captain America: Civil War',director:'Anthony Russo, Joe Russo',cast:'Scarlett Johansson, Elizabeth Olsen, Chris Evans',releaseDate:'May 6, 2016'},  
    {title:'X-Men: Apocalypse',director:'Bryan Singer',cast:'Jennifer Lawrence, Olivia Munn, Oscar Isaac',releaseDate:'May 27, 2016'},  
]
items: item[] = [{name: 'One', val: 1}, {name: 'Two', val: 2}, {name: 'Three', val: 3}];  
selectedValue: string= 'One';  
imgUrl="https://static.javatpoint.com/tutorial/angular7/images/angular-7-logo.png"; 
fullname:string="";
buy:number=0;
sum(){
  this.buy=this.buy+1;
}
div(){
  if(this.buy<0)
    this.buy=this.buy-1;
  else
    this.buy=0;
}
onSave($event)
{
  console.log("Save is successfully",$event);
  
}
onDetail(value:string)
{
  console.log("Is information",value);
  
}
}
class Movie{
  title:string;
  director:string;
  cast:string;
  releaseDate : string;
}
class item {  
  name: string;  
  val: number;  
}  
