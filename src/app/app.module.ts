import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppComponent } from './app.component';
import { BookAppComponent } from './book-app/book-app.component';
import { CarAppComponent } from './car-app/car-app.component';
import {RouterModule} from '@angular/router';
import { Page404Component } from './page404/page404.component';
import { HomepageComponent } from './homepage/homepage.component';
import {ChildAComponent} from './child-a/child-a.component';
import {ChildBbComponent} from './child-bb/child-bb.component';
import { ParamIdComponent } from './param-id/param-id.component';
import { StudentDetailComponent } from './student-detail/student-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    BookAppComponent,
    CarAppComponent,
    Page404Component,
    HomepageComponent,
    ChildAComponent,
    ChildBbComponent,
    ParamIdComponent,
    StudentDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      {path:'students',component:ParamIdComponent},
      {path:'students/:id',component:StudentDetailComponent},
      {path:'book-app',component: BookAppComponent,
      children:[
        {
          path:'child-a',component:ChildAComponent
        },
        {
          path:'child-b',component:ChildBbComponent
        },
      ],
      },
      {path:'car-app',component: CarAppComponent},
      {path: '', redirectTo: '', pathMatch: 'full',component:HomepageComponent
      },
      {path: '**', component:  Page404Component}
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
