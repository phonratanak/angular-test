import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-param-id',
  templateUrl: './param-id.component.html',
  styleUrls: ['./param-id.component.css']
})
export class ParamIdComponent implements OnInit {
  public students=[
    {'id':1,'name':'khmer'},
    {'id':2,'name':'thia'},
    {'id':3,'name':'korean'},
    {'id':4,'name':'china'},
    {'id':5,'name':'japan'},
  ];
  public adds={
    'id':'',
    'name':''}
  constructor(private router:Router) { 
    
  }

  ngOnInit(): void {
  }
  onClick(students)
  {
    this.router.navigate(['/students',students.id]);
   
  }
  add(add){
    if(add.id==="" || add.name==="")
    {
      alert("Please input file");
    }
    else
    {
      this.students.length
      this.students.push({'id':add.id,'name':add.name});
      add.id="";
      add.name="";
    }

  }

  delete(id)
  {
    let filtered = this.students.filter((value)=>value.id !=id);
    this.students=filtered;
  }
  
}
