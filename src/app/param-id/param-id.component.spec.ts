import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParamIdComponent } from './param-id.component';

describe('ParamIdComponent', () => {
  let component: ParamIdComponent;
  let fixture: ComponentFixture<ParamIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParamIdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParamIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
