import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildBbComponent } from './child-bb.component';

describe('ChildBbComponent', () => {
  let component: ChildBbComponent;
  let fixture: ComponentFixture<ChildBbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildBbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildBbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
